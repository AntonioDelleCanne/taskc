DELLE CANNE

Ho tradotto tre elementi dell'applicaizone java: il Rettangolo(Rectangel), le Coordinate(classe Coordinates), 
la Mappa(creata da GameMapFactory e usata tramite l'interfaccia IModifiableGameMap).
In general si trovano classi e interfacce mock, di cui ho implementato siolo la parte a me necessaria.
La cartella util contiene la traduzione di Coordinates e di Rectangle del package util del progetto java.
La cartella map contiene la traduzione delle classi e interfacce all'interno del package model.map del progetto java.
Ho dovuto utilizzare valori null al polsto degli Optional per via della mancanza di questi ultimi in c#.
Ho fatto ampio uso delle proprietà sia nelle classi che nelle interfacce.
Nela cartella UnitTest si una classe di test per ognuno degli elementi tradotti(Rettangolo,Coordiante,Mappa). 


AMERI

Nella realizzazione del mio progetto in C# ho potuto constatare di come i linguaggio non permetta di creare 
delle enumerazioni con metodi e costruttori. Per questo motivo ho "unito" le due enumerazioni, 
UnitType e GenericUnitType, della mia parte in java, in una singola enum, UnitType, in C#.


FERRETTI

Siccome C# non consente di creare enumerazioni con metodi e costruttori ho dovuto creare delle classi qualora
avessi usato delle enumerazioni. Inoltre ho modificato il controllo per verificare il vincitore che non passa
più per il completamento dell'obiettivo ma controlla che sia l'ultimo giocatore rimasto, in quanto per
implementare l'obiettivo avrei avuto bisogno di aggiungere delle parti collegate alla mappa che non erano di mia
competenza.