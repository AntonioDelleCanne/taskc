﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskCSharpDelleCanne;
using System.Collections.Generic;
using System.Linq;
using System;
using TaskCSharpDelleCanne.source.util;

namespace UnitTest.Test
{
    [TestClass]
    public class RectangleTest
    {
        private readonly Func<Coordinates, bool> f = c => c.X % 2 == 0;
        private Rectangle<bool> rect;
        

        [TestInitialize]
        public void TestInitialize()
        {
            rect = new Rectangle<bool>(4, 3, f);
        }

        [TestMethod]
        public void TestEquals()
        {
            Assert.IsTrue(rect.Equals(new Rectangle<bool>(4, 3, f)));
        }

        [TestMethod]
        public void TestBasics()
        {
            Coordinates c = new Coordinates(1, 0);
            Assert.IsTrue(rect[c] == f.Invoke(c));
            foreach (KeyValuePair<Coordinates, bool> e in rect.ToMap())
            {
                Assert.IsTrue(e.Value == f.Invoke(e.Key));
            }
            Assert.IsTrue(rect.Width == 4);
            Assert.IsTrue(rect.Height == 3);

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestOutOfRangeCoordinates()
        {
            var i = rect[new Coordinates(rect.Width + 1, rect.Height)];
        }

        
        

        [TestMethod]
        public void TestSubrectangle()
        {
            bool f1(Coordinates c) => c.Y % 2 == 0;
            Coordinates upright = new Coordinates(2,2);
            Coordinates downleft = new Coordinates(0, 0);
            Rectangle<bool> newSubrect = new Rectangle<bool>(upright.X - downleft.X, upright.Y - downleft.Y, f1);
            Rectangle<bool> expected = new Rectangle<bool>(upright.X - downleft.X, upright.Y - downleft.Y, f);
            Assert.IsTrue(rect[downleft, upright].Equals(expected));
            rect[downleft, upright] = newSubrect;
            Assert.IsTrue(rect[downleft, upright].Equals(newSubrect));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestBadConstraintsForSubrectangle()
        {
            bool f1(Coordinates c) => c.Y % 2 == 0;
            Coordinates upright = new Coordinates(2, 2);
            Coordinates downleft = new Coordinates(0, 0);
            Rectangle<bool> newSubrect = new Rectangle<bool>(upright.X - downleft.X, upright.Y - downleft.Y, f1);
            rect[downleft + new Coordinates(1, 0), upright] = newSubrect;
        }
    }
}
