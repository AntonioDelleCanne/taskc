﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskCSharpDelleCanne;
using TaskCSharpDelleCanne.source.map;
using System.Collections.Generic;
using System.Linq;
using System;
using TaskCSharpDelleCanne.source.general;
using TaskCSharpDelleCanne.source.util;

namespace UnitTest.Test
{
    [TestClass]
    public class MapTest
    {

        private readonly IGameMapFactory mapFactory = new GameMapFactory();
        private IModifiableGameMap map;
        private Player player;

        [TestInitialize]
        public void TestInitialize()
        {
            player = new Player("Antonio", 0);
            int width = 14;
            int height = 10;
            var bg = new Rectangle<ITerrain>(width, height,
               p=> new WaterTerrain());
            var island = new Rectangle<ITerrain>(width - 6, height - 6,
               p => new BasicTerrain());
            bg[new Coordinates(2, 2), new Coordinates(width - 4, height - 4)] = island;
            var background = bg.ToMap();
            map = mapFactory.GetEmptyMapFromBackground(background, new KeyValuePair<int, int>(width, height));
        }

        [TestMethod]
        public void TestGetters()
        {
            Assert.IsTrue(map.GetTerrain(new Coordinates(0,0)) is WaterTerrain);
            Assert.IsTrue(map.GetTerrain(new Coordinates(3, 3)) is BasicTerrain);
            Assert.IsTrue(map.GetStructure(new Coordinates(0, 0)) == null);
            Assert.IsTrue(map.GetUnit(new Coordinates(0, 0)) == null);
        }

        [TestMethod]
        public void TestSetters()
        {
            var harborCord = new Coordinates(0,0);
            var unitCord = new Coordinates(4, 4);
            var ownableCords = new Coordinates(5, 5);
            var unit = new Unit(player);
            var harbor = new Harbor();
            var ownable = new OwnableStructure();

            Assert.IsTrue(map.GetStructure(harborCord) == null);
            map.SetStructure(harborCord, harbor);
            Assert.IsTrue(map.GetStructure(harborCord).Equals(harbor));
            Assert.IsTrue(map.GetStructure(ownableCords) == null);
            map.SetStructure(ownableCords, ownable);
            Assert.IsTrue(map.GetStructure(ownableCords).Equals(ownable));

            Assert.IsTrue(map.GetUnit(unitCord) == null);
            Assert.IsTrue(map.CanStep(unitCord, unit));
            map.SetUnit(unitCord, unit);
            Assert.IsTrue(map.GetUnit(unitCord).Equals(unit));
            map.RemoveUnit(unitCord);
            Assert.IsTrue(map.GetUnit(unitCord) == null);
            Assert.IsTrue(map.CanStep(harborCord, unit));
            map.SetUnit(harborCord, unit);
            Assert.IsTrue(map.GetUnit(harborCord) is Boat boat && boat.Passenger.Equals(unit));
            map.RemoveUnit(harborCord);
            Assert.IsTrue(map.GetUnit(harborCord) == null);
            Assert.IsTrue(map.CanStep(ownableCords, unit));
            map.SetUnit(ownableCords, unit);
            Assert.IsTrue(map.GetStructure(ownableCords) is OwnableStructure ownableStructure && ownableStructure.Conqueror.Equals(unit.Owner));
            ((IOwnableStructure)map.GetStructure(ownableCords)).EndConquer();
            Assert.IsTrue(map.GetStructure(ownableCords) is OwnableStructure oStructure && oStructure.Conqueror == null && oStructure.Owner.Equals(unit.Owner));

            map.RemoveStructure(harborCord);
            Assert.IsTrue(map.GetStructure(harborCord) == null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestIllegalRemove()
        {
            map.RemoveUnit(new Coordinates(1, 1));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestIllegalSet()
        {
            var unitCord = new Coordinates(4, 4);
            var unit = new Unit(player);

            Assert.IsTrue(map.CanStep(unitCord, unit));
            map.SetUnit(unitCord, new Unit(player));
            Assert.IsFalse(map.CanStep(unitCord, unit));
            map.SetUnit(unitCord, new Unit(player));
        }

        [TestMethod]
        public void TestToMap()
        {
            var dict = map.ToMap();
            var cords = new Coordinates(0, 0);
            Assert.IsTrue(dict[cords].Count == 1);
            Assert.IsTrue(map.GetTerrain(cords).Equals(dict[cords][0]));

        }
    }
}
