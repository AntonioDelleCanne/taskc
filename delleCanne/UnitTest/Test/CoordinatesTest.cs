using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskCSharpDelleCanne;
using System.Collections.Generic;
using System.Linq;
using System;

namespace UnitTest.Test
{

    [TestClass]
    public class CoordinatesTest
    {

        private Coordinates c1;
        private Coordinates c2;

        [TestInitialize]
        public void TestInitialize()
        {
            c1 = new Coordinates(1, 2);
            c2 = new Coordinates(2, 3);
        }

        [TestMethod]
        public void TestOperations()
        {
            //sum
            Assert.IsTrue((c1 + c2).Equals(new Coordinates(3,5)));
            //subtraction
            Assert.IsTrue((c1 +(-c2)).Equals(new Coordinates(-1, -1)));

        }

        [TestMethod]
        public void TestUtilityMethods()
        {
            var result = Coordinates.GetCoordinatesRange(c1, 1, 4, 3);
            var expected = new HashSet<Coordinates>() { new Coordinates(2,2), new Coordinates(0, 2),
                new Coordinates(0, 1), new Coordinates(1, 1) , new Coordinates(2, 1)};
            Assert.IsTrue(result.Count() == expected.Count());
            Assert.IsTrue(result.Intersect(expected).Count() == result.Count());
            Assert.IsTrue(Coordinates.Distance(c1, c2) == 1);
        }
    }
}
