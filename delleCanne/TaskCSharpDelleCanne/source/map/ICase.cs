﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskCSharpDelleCanne.source.general;

namespace TaskCSharpDelleCanne.source.map
{
    internal interface ICase
    {
        IUnit Unit { get; set; }

        IStructure Structure { get; set; }

        ITerrain Terrain { get; }

        bool CanStep(IUnit unit);

    }
}
