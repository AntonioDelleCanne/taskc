﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskCSharpDelleCanne.source.general;

namespace TaskCSharpDelleCanne.source.map
{
    public interface IObservableGameMap
    {

        /**
         * 
         * @return the size of the game map
         */
        KeyValuePair<int, int> Size { get; }

        /**
        * @param cords the cords of the case to be checked for the terrain
        * 
        * @return the terrain of the case
        */
        ITerrain GetTerrain(Coordinates cords);

        /**
         * @param cords the cords of the case to be checked for the structure
         * 
         * @return the structure associated with this terrain if present, Optional.Empty
         *         otherwise
         */
        IStructure GetStructure(Coordinates cords);

        /**
         * @param cords the cords to be checked for the pg
         * 
         * @return the Unit on the case if present
         */
        IUnit GetUnit(Coordinates cords); 

        /**
         * 
         * @return the the actual updated state of the game map
         */
        Dictionary<Coordinates, List<IGameObject>> ToMap();
    }
}
