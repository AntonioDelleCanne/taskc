﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskCSharpDelleCanne.source.general;

namespace TaskCSharpDelleCanne.source.map
{
    public interface IGameMapFactory
    {
        IModifiableGameMap GetEmptyMapFromBackground(IDictionary<Coordinates, ITerrain> background, KeyValuePair<int, int> size);
    }
}
