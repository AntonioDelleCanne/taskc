﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using TaskCSharpDelleCanne.source.general;

namespace TaskCSharpDelleCanne.source.map
{
    internal class Case : ICase
    {
        private IUnit unit;
        private IStructure structure;
        public ITerrain Terrain { get; }
        public IUnit Unit {
            get => unit;
            set
            {
                if(value == null)//remove
                {
                    if (Unit == null) throw new InvalidOperationException();
                    unit = value;
                } else//set
                {
                    if (Unit != null) throw new InvalidOperationException();
                    IUnit validUnit = value;
                    if (!CanStep(value))
                    {
                        if (value is IVehicle vehicle && CanStep(vehicle.Passenger)) {
                            validUnit = vehicle.Passenger;
                        } else {
                            throw new ArgumentException();
                        }
                    }
                    this.unit = validUnit;
                    OnStep(validUnit);
                }
            }
        }
        public IStructure Structure
        {
            get => structure;
            set
            {
                if (value == null)//remove
                {
                    if (Structure == null) throw new InvalidOperationException();

                }
                else//set
                {
                    if (Structure != null) throw new InvalidOperationException();
                    if (!value.CaneBeBuilt(Terrain)) throw new ArgumentException();
                }
                structure = value;
            }
        }




        public Case(ITerrain terrain)
        {
            Terrain = terrain;
        }

        public bool CanStep(IUnit unit)
        {
            if (Unit != null)
            {
                return false;
            }
            if (!(Structure != null && Structure is IVehicleProducer)
                && Terrain.GetRequiredAbilities().Intersect(unit.GetAbilities()).Count() != Terrain.GetRequiredAbilities().Count()) {
                return false;
            }
            return true;
        }

        // stuff to do when a unit is set on the case:
        // 1.conquer the structure if conquerable
        // 2.step on a vehicle if there is a vehicle producer
        private void OnStep(IUnit unit)
        {
            if (Unit == null)
            {
                throw new InvalidOperationException();
            }
            if (!Unit.Equals(unit))
            {
                throw new ArgumentException();
            }
            if (Structure != null)
            {
                if (Structure is IVehicleProducer vehicleProducer) {
                    if (unit is IVehicle) {
                        this.unit = vehicleProducer.GetVehicle(((IVehicle)unit).Passenger);

                    } else {
                        this.unit = vehicleProducer.GetVehicle(unit);
                    }
                } else if (Structure is IOwnableStructure oStructure)
                {
                    if (Structure.Owner == null
                            || !Structure.Owner.Equals(unit.Owner))
                    {
                        oStructure.InitiateConquer(unit.Owner);
                    }
                }
            }
        }
    }
}
