﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskCSharpDelleCanne.source.general;

namespace TaskCSharpDelleCanne.source.map
{
    public class GameMapFactory : IGameMapFactory
    {
        public IModifiableGameMap GetEmptyMapFromBackground(IDictionary<Coordinates, ITerrain> background, KeyValuePair<int, int> size)
        {
            CaseFactory caseFactory = new CaseFactory();
            var mapBase = new Dictionary<Coordinates, ICase>();
            foreach (KeyValuePair<Coordinates, ITerrain> entry in background)
            {
                mapBase.Add(entry.Key, caseFactory.GetEmptyCase(entry.Value));
            }
            return new GameMap(size, () => mapBase);
        }
    }
}
