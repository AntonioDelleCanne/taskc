﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskCSharpDelleCanne.source.general;

namespace TaskCSharpDelleCanne.source.map
{
    public interface IModifiableGameMap : IObservableGameMap
    {

        /**
         * Builds the selected structure on the terrain at the given cords.
         * 
         * @param cords     the cords of the case to be modified
         * 
         * @param structure the structure to be built
         */
        void SetStructure(Coordinates cords, IStructure structure);

        /**
         * Removes the structure specific for the terrain.
         * 
         * @param cords the cords of the case of the structure to be removed
         * 
         * @throws IllegalArgumentException if there isn't a structure in this case
         */
        void RemoveStructure(Coordinates cords);

        /**
         * @param cords the cords of the case in which the unit wants to step
         * @param unit  the unit that tries to step on the case
         * @return whether the passed unit can step on the case
         */
        bool CanStep(Coordinates cords, IUnit unit);

        /**
         * Make the unit step on a case.
         * When stepping on a case, the unit conquers any structure on it if present and
         * steps on a vehicle if a vehicle producer is there.
         * 
         * @param cords the cords of the case in which the structure has to be conquered
         * 
         * @param unit  to set on the case
         * 
         * @throws IllegalArgumentException if the Unit can't step on the case or the case is already occupied by another Unit
         */
        void SetUnit(Coordinates cords, IUnit unit);

        /**
         * removes the unit from the case in position cords.
         * 
         * @param cords the cords of the case in which to remove the unit
         * @throws IllegalArgument  Exception if there is no Unit in the case
         */
        void RemoveUnit(Coordinates cords);
    }
}
