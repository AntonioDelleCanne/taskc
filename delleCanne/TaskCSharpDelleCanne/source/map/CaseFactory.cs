﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskCSharpDelleCanne.source.general;

namespace TaskCSharpDelleCanne.source.map
{
    internal class CaseFactory
    {
        public Case GetEmptyCase(ITerrain caseTerrain)
        {
            return new Case(caseTerrain);
        }
    }
}
