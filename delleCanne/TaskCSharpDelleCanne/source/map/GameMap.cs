﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskCSharpDelleCanne.source.general;

namespace TaskCSharpDelleCanne.source.map
{
    internal class GameMap : IModifiableGameMap
    {
        private readonly IDictionary<Coordinates, ICase> map;

        public KeyValuePair<int, int> Size { get; }

        public GameMap(KeyValuePair<int, int> size,  Func<IDictionary<Coordinates, ICase>> getMap)
        {
            Size = size;
            map = getMap.Invoke();
        }

        //protected abstract IDictionary<Coordinates, ICase> GenerateGameMap();

        public bool CanStep(Coordinates cords, IUnit unit)
        {
            return map[cords].CanStep(unit);
        }

        public IStructure GetStructure(Coordinates cords)
        {
            return map[cords].Structure;
        }

        public ITerrain GetTerrain(Coordinates cords)
        {
            return map[cords].Terrain;
        }

        public IUnit GetUnit(Coordinates cords)
        {
            return map[cords].Unit;
        }

        public void RemoveStructure(Coordinates cords)
        {
            RunIllegalSateToIllegalArgument(() => map[cords].Structure = null);
        }

        public void RemoveUnit(Coordinates cords)
        {
            RunIllegalSateToIllegalArgument(() => map[cords].Unit = null);
        }

        public void SetStructure(Coordinates cords, IStructure structure)
        {
            RunIllegalSateToIllegalArgument(() => map[cords].Structure = structure);
        }

        public void SetUnit(Coordinates cords, IUnit unit)
        {
            RunIllegalSateToIllegalArgument(() => map[cords].Unit = unit);
        }

        public Dictionary<Coordinates, List<IGameObject>> ToMap()
        {
            var result = new Dictionary<Coordinates, List<IGameObject>>();
            foreach (var entry in this.map)
            {
                var l = new List<IGameObject>();
                l.Add(entry.Value.Terrain);
                if(entry.Value.Structure != null) l.Add(entry.Value.Structure);
                if (entry.Value.Unit != null) l.Add(entry.Value.Unit);
                result.Add(entry.Key, l);
            }
            return result;
        }

        private void RunIllegalSateToIllegalArgument(Action action)
        {
            try
            {
                action.Invoke();
            }
            catch (InvalidOperationException e)
            {
                throw new ArgumentException(e.Message);
            }
        }
    }
}
