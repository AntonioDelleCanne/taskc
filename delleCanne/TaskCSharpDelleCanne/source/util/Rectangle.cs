﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskCSharpDelleCanne.source.util
{
    public class Rectangle<X>
    {
        private readonly Dictionary<Coordinates, X> grid;

        public int Width { get; }
        public int Height { get; }

        public Rectangle(int width, int height, Func<Coordinates, X> elements)
        {
            grid = new Dictionary<Coordinates, X>();
            Width = width;
            Height = height;
            
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Coordinates cords = new Coordinates(i, j);
                    grid.Add(cords, elements.Invoke(cords));
                }
            }
        }
        public X this[Coordinates pos]
        {
            get
            {
                if (CoordiantesOutOfRange(pos)) throw new ArgumentOutOfRangeException();
                return grid[pos];
            }
            set
            {
                if (CoordiantesOutOfRange(pos)) throw new ArgumentOutOfRangeException();
                grid[pos] = value;
            }
        }

        public Rectangle<X> this[Coordinates downleft, Coordinates upright]
        {
            get
            {
                if (CoordiantesOutOfRange(upright)
                || CoordiantesOutOfRange(downleft))
                {
                    throw new ArgumentOutOfRangeException();
                }
                int width = upright.X - downleft.X;
                int height = upright.Y - downleft.Y;
                Rectangle<X> result = new Rectangle<X>(width, height, (p) => this[p]);
                for (int i = 0; i < width; i++)
                {
                    for (int j = 0; j < height; j++)
                    {
                        Coordinates cords = new Coordinates(i, j);
                        result[cords] = this[cords + downleft];
                    }
                }
                return result;
            }
            set
            {
                if (upright.X - downleft.X != value.Width || upright.Y - downleft.Y != value.Height)
                {
                    throw new ArgumentException();
                }
                if (CoordiantesOutOfRange(upright)
                || CoordiantesOutOfRange(new Coordinates(downleft.X + value.Width,
                        downleft.Y + value.Height)))
                {
                    throw new ArgumentOutOfRangeException();
                }
                foreach (KeyValuePair<Coordinates, X> entry in value.ToMap().AsEnumerable())
                {
                    this[entry.Key + downleft] = entry.Value; 
                }
            }
        }

        public Dictionary<Coordinates, X> ToMap()
        {
            return new Dictionary<Coordinates, X>(this.grid);
        }

        

        private bool CoordiantesOutOfRange(Coordinates position)
        {
            if (position.X >= Width || position.X < 0 || position.Y >= Height || position.Y < 0)
            {
                return true;
            }
            return false;
        }

        public override bool Equals(object obj)
        {
            var rectangle = obj as Rectangle<X>;
            var dic1 = this.ToMap();
            var dic2 = rectangle.ToMap();
            return rectangle != null &&
                   (dic1 == dic2) || //Reference comparison (if both points to same object)
             (dic1.Count == dic2.Count && !dic1.Except(dic2).Any()) &&
                   Width == rectangle.Width &&
                   Height == rectangle.Height;
        }

        public override int GetHashCode()
        {
            var hashCode = 1423229466;
            hashCode = hashCode * -1521134295 + EqualityComparer<Dictionary<Coordinates, X>>.Default.GetHashCode(grid);
            hashCode = hashCode * -1521134295 + Width.GetHashCode();
            hashCode = hashCode * -1521134295 + Height.GetHashCode();
            return hashCode;
        }
    }
}
