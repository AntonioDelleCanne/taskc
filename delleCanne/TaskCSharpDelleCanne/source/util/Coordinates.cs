﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskCSharpDelleCanne
{
    public class Coordinates
    {
        public int X { get; }
        public int Y { get; }

        public Coordinates(int x, int y)
        {
            X = x;
            Y = y;
        }


        public static Coordinates operator+(Coordinates c1, Coordinates c2)
        {
            return new Coordinates(c1.X + c2.X, c1.Y + c2.Y);
        }

        public static Coordinates operator-(Coordinates c)
        {
            return new Coordinates(-c.X, -c.Y);
        }

        public static int Distance(Coordinates c1, Coordinates c2)
        {
            return (int)Math.Max(Math.Abs(c1.X - c2.X), Math.Abs(c1.Y - c2.Y));
        }

        public static HashSet<Coordinates> GetCoordinatesRange(Coordinates cords, int distance, int limitX,
            int limitY)
        {
            var result = new List<Coordinates>();
            for (int i = 1; i <= distance; i++)
            {
                for (int j = 1; j <= distance; j++)
                {
                    result.Add(new Coordinates(cords.X + j, cords.Y));
                    result.Add(new Coordinates(cords.X - j, cords.Y));
                    result.Add(new Coordinates(cords.X, cords.Y - i));
                    result.Add(new Coordinates(cords.X, cords.Y + i));
                    result.Add(new Coordinates(cords.X + j, cords.Y + i));
                    result.Add(new Coordinates(cords.X - j, cords.Y + i));
                    result.Add(new Coordinates(cords.X + j, cords.Y - i));
                    result.Add(new Coordinates(cords.X - j, cords.Y - i));
                }
            }
            return new HashSet<Coordinates>(result.Where(c => limitX > c.X && c.X >= 0 && limitY > c.Y && c.Y >= 0));
        }

        public override bool Equals(object obj)
        {
            var coordinates = obj as Coordinates;
            return coordinates != null &&
                   X == coordinates.X &&
                   Y == coordinates.Y;
        }

        public override int GetHashCode()
        {
            var hashCode = 1861411795;
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            return hashCode;
        }

        public override string ToString()
        {
            return "Coordinates [x=" + X + ", y=" + Y + "]";
        }
    }
}
