﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskCSharpDelleCanne.source.general
{
    //mock
    public class Player
    {
        public string Name { get; }
        public int Id { get; }

        public Player(string name, int id)
        {
            this.Name = name;
            this.Id = id;
        }
    }
}
