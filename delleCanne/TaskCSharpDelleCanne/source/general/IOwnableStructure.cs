﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskCSharpDelleCanne.source.general
{
    //mock
    public interface IOwnableStructure : IStructure
    {
        void InitiateConquer(Player player);

        Player Conqueror { get; }

        void EndConquer();
    }
}
