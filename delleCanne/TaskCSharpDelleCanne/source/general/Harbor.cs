﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskCSharpDelleCanne.source.general
{
    //mock
    public class Harbor : IVehicleProducer
    {
        public Player Owner { get; set; }

        public bool CaneBeBuilt(ITerrain terrain)
        {
            return true;
        }

        public IVehicle GetVehicle(IUnit unit)
        {
            return new Boat(unit);
        }
    }
}
