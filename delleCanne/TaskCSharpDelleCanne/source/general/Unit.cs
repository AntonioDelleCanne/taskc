﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskCSharpDelleCanne.source.general
{
    //mock
    public class Unit : IUnit
    {
        public Player Owner { get; set; }

        public Unit(Player owner)
        {
            Owner = owner;
        }

        public HashSet<object> GetAbilities()
        {
            return new HashSet<object>();
        }
    }
}
