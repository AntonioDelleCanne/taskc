﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskCSharpDelleCanne.source.general
{
    public class OwnableStructure : IOwnableStructure
    {
        public Player Owner { get; set; }

        public Player Conqueror { get; private set; }

        public bool CaneBeBuilt(ITerrain terrain)
        {
            return true;
        }

        public void EndConquer()
        {
            Owner = Conqueror;
            Conqueror = null;
        }

        public void InitiateConquer(Player player)
        {
            Conqueror = player;
        }
    }
}
