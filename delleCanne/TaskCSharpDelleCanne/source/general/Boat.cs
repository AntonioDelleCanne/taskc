﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskCSharpDelleCanne.source.general
{
    //mock
    public class Boat : IVehicle
    {
        public IUnit Passenger { get; }
        public Player Owner { get => Passenger.Owner; set => Passenger.Owner = value; }

        public Boat(IUnit unit)
        {
            Passenger = unit;
        }

        public HashSet<object> GetAbilities()
        {
            return new HashSet<object>();
        }
    }
}
