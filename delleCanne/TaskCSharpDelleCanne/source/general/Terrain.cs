﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskCSharpDelleCanne.source.general
{
    //mock
    public interface ITerrain : IGameObject
    {
        HashSet<object> GetRequiredAbilities();
    }
}
