﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PoleisKriegAmeri.unit
{
    public enum UnitType
    {
        CLOSE_BASIC,
        DISTANCE_BASIC,
        CLOSE_NORMAL,
        DISTANCE_NORMAL,
        HERO_CLOSE_FIGHTER,
        HERO_DISTANCE_FIGHTER,
        TERRAIN_VEHICLE,
        WATER_VEHICLE,
        AIR_VEHICLE
    }
}
