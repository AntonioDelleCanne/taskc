﻿using System;
using System.Collections.Generic;
using System.Text;
using PoleisKriegAmeri.cost;

/*
 * I metodi relativi alle abilità (IAbility), li ho commentati e non verranno implementati perchè riguardano una parte di progetto
 * non svolta da me.
 */
namespace PoleisKriegAmeri.unit
{
    public interface IUnit : IGameObject
    {
        string Name { get; }
        UnitType UnitType { get; }
        int Strength { get; }
        int Hp { get; set; }
        int InitialHp { get; }
        bool IsDead();
        int AttackRange { get; }
        int MovementRange { get; }
        void Move();
        bool CanMove();
        void Attack();
        bool CanAttack();
        bool MovesAfterKill();
        void TakeDamage(int opponentsStrength);
        void Reset();
        ICost Cost { get; }
        string CostToString { get; }
        ICost UnlockCost { get; }
        string UnlockCostToString { get; }
        //Set<IAbility> Abilities { get; }
        //void AddAbility(IAbility ability);
        //void RemoveAbility(IAbility ability);
    }
}
