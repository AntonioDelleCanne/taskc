﻿using System;
using System.Collections.Generic;
using System.Text;
using PoleisKriegAmeri.cost;

/*
 *  In questa versione le unità non presenteranno la possibilità di essere possedute da un giocatore e non avranno le abilità 
 *  in quanto comprendevano interfaccie e classi da me non implementate.
 */
namespace PoleisKriegAmeri.unit
{

    public class Unit : IUnit
    {
        private readonly string _name;
        private readonly int _strength;
        private readonly int _movementRange;
        private readonly int _attackRange;
        private readonly int _initialHp;
        private readonly int possibleAttack;
        private readonly bool canMoveAfterAttack;
        private readonly bool moveOnKill;
        private readonly UnitType _unitType;
        private readonly ICost _unitCost;
        private readonly ICost _unitUnlockCost;
        private bool movedAfterAttack;
        private bool didFirstMovement;
        private int attackCount;

        public string Name => _name;
        public int Strength => _strength;
        public int Hp { get; set; }
        public int InitialHp => _initialHp;
        public int AttackRange => _attackRange;
        public int MovementRange => _movementRange;
        public UnitType UnitType => _unitType;
        public ICost Cost => _unitCost;
        public string CostToString => Cost.ToString();
        public ICost UnlockCost => _unitUnlockCost;
        public string UnlockCostToString => UnlockCost.ToString();
        public string Description => this.Name + IsHero()
                                    + "\nStrength: " + this.Strength
                                    + "\nHp: " + this.Hp + "/" + this._initialHp
                                    + "\nMovement Range: " + this.MovementRange
                                    + "\nAttack Range: " + (this.AttackRange)
                                    + "\nRemainig Attacks: " + (this.possibleAttack - this.attackCount)
                                    + "\nCan move: " + CanMove()
                                    + "\nCan move after attacking: " + this.canMoveAfterAttack
                                    + "\nMove on kill: " + this.moveOnKill;

        public Unit(string name, int strength, int initialHp,
            int movementRange, int attackRange, int possibleAttack, bool canMoveAfterAttack,
            bool moveOnKill, ICost unitCost, ICost unitUnlockCost, UnitType unitType)
        {
            this._name = name;
            this._strength = strength;
            this._initialHp = initialHp;
            this.Hp = initialHp;
            this._movementRange = movementRange;
            this._attackRange = attackRange;
            this.possibleAttack = possibleAttack;
            this.canMoveAfterAttack = canMoveAfterAttack;
            this.moveOnKill = moveOnKill;
            this.movedAfterAttack = true;
            this.didFirstMovement = true;
            this.attackCount = possibleAttack;
            this._unitCost = unitCost;
            this._unitUnlockCost = UnlockCost;
            this._unitType = unitType;
        }

        public bool IsDead()
        {
            return Hp <= 0;
        }

        public void Move()
        {
            if (CanMove())
            {
                if (!this.didFirstMovement)
                {
                    this.didFirstMovement = true;
                }
                else if (this.canMoveAfterAttack)
                {
                    this.movedAfterAttack = true;
                    this.attackCount = this.possibleAttack;
                }
            }
        }

        public bool CanMove()
        {
            return (!this.didFirstMovement && this.attackCount == 0)
                || (this.canMoveAfterAttack && !this.movedAfterAttack && this.attackCount != 0);
        }

        public void Attack()
        {
            if (CanAttack())
            {
                if (!this.didFirstMovement)
                {
                    this.didFirstMovement = true;
                }
                this.attackCount++;
                if (this.canMoveAfterAttack)
                {
                    this.movedAfterAttack = false;
                }
            }
        }

        public bool CanAttack()
        {
            return possibleAttack > attackCount;
        }

        public bool MovesAfterKill()
        {
            return this.moveOnKill;
        }

        public void TakeDamage(int opponentsStrength)
        {
            if (this.Hp < opponentsStrength)
            {
                Hp = 0;
            }
            else
            {
                 Hp -= opponentsStrength;
            }
        }

        public void Reset()
        {
            this.didFirstMovement = false;
            if (this.canMoveAfterAttack)
            {
                this.movedAfterAttack = true;
            }
            this.attackCount = 0;
        }

        private string IsHero()
        {
            return (this._unitType.Equals(UnitType.HERO_CLOSE_FIGHTER) 
                || this._unitType.Equals(UnitType.HERO_DISTANCE_FIGHTER))
                    ? ((UnitType.Equals(UnitType.HERO_CLOSE_FIGHTER)) ? "\n(Close Hero)" : "\n(Distance Hero)")
                    : "";
        }
    }
}
