﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PoleisKriegAmeri.cost;
using PoleisKriegAmeri.player;

namespace PoleisKriegAmeri.unit
{
    internal class Level2Unit : Unit
    {
        private static readonly UnitType TYPE = UnitType.DISTANCE_BASIC;
        private static readonly string NAME = "Basic Distance Soldier";
        private static readonly int STRENGTH = 15;
        private static readonly int HP = 15;
        private static readonly int MOV_RANGE = 1;
        private static readonly int ATT_RANGE = 2;
        private static readonly int POSSIBLE_ATT = 1;
        private static readonly int GOLD_COST = 80;
        private static readonly int WOOD_COST = 80;
        private static readonly int POPULATION_COST = 1;
        private static readonly bool CAN_MOVE_AFTER_ATTACK = false;
        private static readonly bool MOVE_ON_KILL = false;

        public Level2Unit(IPlayer owner) : base(NAME, STRENGTH + owner.PlayerRace.GetStrBoost(TYPE),
                    HP + owner.PlayerRace.GetHpBoost(TYPE), MOV_RANGE + owner.PlayerRace.GetMovRangeBoost(TYPE),
                    ATT_RANGE + owner.PlayerRace.GetAttRangeBoost(TYPE),
                    POSSIBLE_ATT + owner.PlayerRace.GetPossibleAttBoost(TYPE), CAN_MOVE_AFTER_ATTACK, MOVE_ON_KILL,
                    new BasicCost(GOLD_COST, WOOD_COST, POPULATION_COST).MergeBasicCost(owner.PlayerRace.GetCostBoost(TYPE)),
                    new BasicCost(), TYPE)
        { }

        public Level2Unit() : base(NAME, STRENGTH, HP, MOV_RANGE, ATT_RANGE, POSSIBLE_ATT, CAN_MOVE_AFTER_ATTACK,
                    MOVE_ON_KILL, new BasicCost(), new BasicCost(), TYPE)
        { }
    }
}
