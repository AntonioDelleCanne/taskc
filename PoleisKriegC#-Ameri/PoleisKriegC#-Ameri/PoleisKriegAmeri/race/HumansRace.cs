﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PoleisKriegAmeri.cost;
using PoleisKriegAmeri.unit;

namespace PoleisKriegAmeri.race
{
    internal class HumansRace : Race
    {
        private static readonly int BASIC_CLOSE_FIGHTER_HP = +5;
        private static readonly int BASIC_CLOSE_FIGHTER_WOOD = +10;

        private static readonly int BASIC_DISTANCE_FIGHTER_STR = +2;

        private static readonly int NORMAL_CLOSE_FIGHTER_STR = +5;
        private static readonly int NORMAL_CLOSE_FIGHTER_POSSIBLE_ATT = -1;

        private static readonly int NORMAL_DISTANCE_FIGHTER_HP = +10;

        private static readonly int HERO_CLOSE_FIGHTER_HP = +20;
        private static readonly int HERO_CLOSE_FIGHTER_GOLD = +50;

        private static readonly int HERO_DISTANCE_FIGHTER_MOV_RANGE = +1;
        private static readonly int HERO_DISTANCE_FIGHTER_ATT_RANGE = -2;
        private static readonly int HERO_DISTANCE_FIGHTER_POSS_ATT = +1;
        
        public HumansRace() : base("Humans", GetStatsBoostMap(), GetCostBoostMap())
        { }

        private static Dictionary<UnitType, List<int>> GetStatsBoostMap()
        {
            Dictionary<UnitType, List<int>> boostMap = new Dictionary<UnitType, List<int>>();
            boostMap.Add(UnitType.CLOSE_BASIC, new List<int>(new[] { 0, BASIC_CLOSE_FIGHTER_HP, 0, 0, 0 }));
            boostMap.Add(UnitType.DISTANCE_BASIC, new List<int>(new[] { BASIC_DISTANCE_FIGHTER_STR, 0, 0, 0, 0 }));
            boostMap.Add(UnitType.CLOSE_NORMAL, new List<int>(new[] { NORMAL_CLOSE_FIGHTER_STR, 0, 0, 0, NORMAL_CLOSE_FIGHTER_POSSIBLE_ATT}));
            boostMap.Add(UnitType.DISTANCE_NORMAL, new List<int>(new[] { 0, NORMAL_DISTANCE_FIGHTER_HP, 0, 0, 0 }));
            boostMap.Add(UnitType.HERO_CLOSE_FIGHTER, new List<int>(new[] { 0, HERO_CLOSE_FIGHTER_HP, 0, 0, 0 }));
            boostMap.Add(UnitType.HERO_DISTANCE_FIGHTER, new List<int>(new[] { 0, 0, HERO_DISTANCE_FIGHTER_MOV_RANGE,
                    HERO_DISTANCE_FIGHTER_ATT_RANGE, HERO_DISTANCE_FIGHTER_POSS_ATT }));
            return boostMap;
        }

        private static Dictionary<UnitType, ICost> GetCostBoostMap()
        {
            Dictionary<UnitType, ICost> costMap = new Dictionary<UnitType, ICost>();
            costMap.Add(UnitType.CLOSE_BASIC, new BasicCost(0, BASIC_CLOSE_FIGHTER_WOOD, 0));
            costMap.Add(UnitType.DISTANCE_BASIC, new BasicCost(0, 0, 0));
            costMap.Add(UnitType.CLOSE_NORMAL, new BasicCost(0, 0, 0));
            costMap.Add(UnitType.DISTANCE_NORMAL, new BasicCost(0, 0, 0));
            costMap.Add(UnitType.HERO_CLOSE_FIGHTER, new BasicCost(HERO_CLOSE_FIGHTER_GOLD, 0, 0));
            costMap.Add(UnitType.HERO_DISTANCE_FIGHTER, new BasicCost(0, 0, 0));
            return costMap;
        }
    }
}
