﻿using System;
using System.Collections.Generic;
using System.Text;
using PoleisKriegAmeri.cost;
using PoleisKriegAmeri.unit;

namespace PoleisKriegAmeri.race
{
    class Race : IRace
    {
        private static readonly int STR_INDEX = 0;
        private static readonly int HP_INDEX = 1;
        private static readonly int MOV_RANGE_INDEX = 2;
        private static readonly int ATT_RANGE_INDEX = 3;
        private static readonly int POSSIBLE_ATT_INDEX = 4;

        private readonly Dictionary<UnitType, List<int>> unitStatsBoost;
        private readonly Dictionary<UnitType, ICost> unitCostBoost;
        private readonly string _name;

        public Race(string name, Dictionary<UnitType, List<int>> unitStatsBoost, Dictionary<UnitType, ICost> unitCostBoost)
        {
            this._name = name;
            this.unitStatsBoost = unitStatsBoost;
            this.unitCostBoost = unitCostBoost;
        }

        private int GetBoost(UnitType unitType, int index)
        {
            return unitStatsBoost[unitType][index];
        }

        public string RaceName => this._name;

        public virtual int GetAttRangeBoost(UnitType unitType)
        {
            return GetBoost(unitType, ATT_RANGE_INDEX);
        }

        public virtual ICost GetCostBoost(UnitType unitType)
        {
            return unitCostBoost[unitType];
        }

        public virtual int GetHpBoost(UnitType unitType)
        {
            return GetBoost(unitType, HP_INDEX);
        }

        public virtual int GetMovRangeBoost(UnitType unitType)
        {
            return GetBoost(unitType, MOV_RANGE_INDEX);
        }

        public virtual int GetPossibleAttBoost(UnitType unitType)
        {
            return GetBoost(unitType, POSSIBLE_ATT_INDEX);
        }

        public virtual int GetStrBoost(UnitType unitType)
        {
            return GetBoost(unitType, STR_INDEX);
        }
    }
}
