﻿using System;
using System.Collections.Generic;
using System.Text;
using PoleisKriegAmeri.cost;
using PoleisKriegAmeri.unit;

namespace PoleisKriegAmeri.race
{
    interface IRace
    {
        string RaceName { get; }
        int GetStrBoost(UnitType unitType);
        int GetHpBoost(UnitType unitType);
        int GetMovRangeBoost(UnitType unitType);
        int GetAttRangeBoost(UnitType unitType);
        int GetPossibleAttBoost(UnitType unitType);
        ICost GetCostBoost(UnitType unitType);
    }
}
