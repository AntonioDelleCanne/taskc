﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PoleisKriegAmeri.cost;
using PoleisKriegAmeri.player;
using PoleisKriegAmeri.race;
using PoleisKriegAmeri.resource;
using PoleisKriegAmeri.unit;

namespace PoleisKriegAmeri
{
    [TestClass]
    public class Test
    {
        /*
         * Test che verifica il comportamento di un'unità appena creta. L'unità non potrà ne attacare ne 
         * muoversi.
         */
        [TestMethod()]
        public void UnitOnCreationTest()
        {
            var unit = new Level1Unit();
            Assert.AreEqual(unit.UnitType, UnitType.CLOSE_BASIC);
            //Appena creata un'unità non può muovere e combattere.
            Assert.IsFalse(unit.CanMove());
            Assert.IsFalse(unit.CanAttack());
            //Reset dei valori dell'unità 
            unit.Reset();
            //Ora l'unità potrà muoversi e attacare
            Assert.IsTrue(unit.CanMove());
            Assert.IsTrue(unit.CanAttack());
        }

        /*
         * Test che verifica le differenze tra vari tipi di unita e tra unità dello stesso tipo, ma neutrali
         * e non.
         */
        [TestMethod()]
        public void DifferancesBetweenUnitTest()
        {
            var neutralSoldier = new Level1Unit();
            var neutralArcher = new Level2Unit();

            var humanPlayer = new Player("Keivan", 1, new HumansRace());
            var humanSoldier = new Level1Unit(humanPlayer);
            var humanArcher = new Level2Unit(humanPlayer);

            //Verifico che il soldato neutrale e quello umano abbiano valori diversi perchè quello umano ha i valori 
            //modificati in base alla sua razza. L'umano ha Hp e costo di legna maggiore.
            Assert.AreEqual(humanSoldier.UnitType, neutralSoldier.UnitType);
            Assert.AreNotEqual(humanSoldier.Hp, neutralSoldier.Hp);
            Assert.AreEqual(humanSoldier.Hp, 25);
            Assert.AreEqual(neutralSoldier.Hp, 20);
            Assert.AreNotEqual(humanSoldier.Cost.GetCost(), neutralSoldier.Cost.GetCost());
            Assert.AreEqual(humanSoldier.Cost.GetCost()[Resource.WOOD], 110);
            
            //Verifico le differenze tra arcere umano e arcere neutrale. l'umano si differenzia solo per la Str.
            Assert.AreEqual(humanArcher.UnitType, neutralArcher.UnitType);
            Assert.AreEqual(humanArcher.Hp, neutralArcher.Hp);
            Assert.AreNotEqual(humanArcher.Strength, neutralArcher.Strength);
            Assert.AreEqual(humanArcher.Strength, 17);
            Assert.AreEqual(neutralArcher.Strength, 15);


            //Verifico le differenze tra soldati e arceri
            Assert.AreNotEqual(neutralSoldier.UnitType, neutralArcher.UnitType);
            Assert.AreNotEqual(neutralSoldier.AttackRange, neutralArcher.AttackRange);
            Assert.AreNotEqual(neutralSoldier, 1);
            Assert.AreNotEqual(neutralArcher, 2);
        }

        [TestMethod]
        public void UnitAction()
        {
            var soldier = new Level1Unit();
            soldier.Reset();
            //Prima muovo l'unità e poi attacco.
            Assert.IsTrue(soldier.CanAttack());
            Assert.IsTrue(soldier.CanMove());
            soldier.Move();
            Assert.IsFalse(soldier.CanMove());
            Assert.IsTrue(soldier.CanAttack());
            soldier.Attack();
            Assert.IsFalse(soldier.CanAttack());

            soldier.Reset();
            //Prima attacco, poi non potrò muovermi.
            Assert.IsTrue(soldier.CanAttack());
            Assert.IsTrue(soldier.CanMove());
            soldier.Attack();
            Assert.IsFalse(soldier.CanAttack());
            Assert.IsFalse(soldier.CanMove());

            soldier.Reset();
            //Faccio ricevere dei danni all'unità
            Assert.AreEqual(soldier.Hp, 20);
            soldier.TakeDamage(15);
            Assert.AreEqual(soldier.Hp, 5);
            Assert.IsFalse(soldier.IsDead());
            soldier.TakeDamage(15);
            Assert.AreEqual(soldier.Hp, 0);
            Assert.IsTrue(soldier.IsDead());
        }

        [TestMethod]
        public void CostTest()
        {
            var cost1 = new BasicCost(10, 10, 0);
            var cost2 = new BasicCost(100, 100, 100);
            //verifico che il merge di due costi avvenga correttamente
            cost2.MergeBasicCost(cost1);
            Assert.AreEqual(cost2.GetCost()[Resource.GOLD], 110);
            Assert.AreEqual(cost2.GetCost()[Resource.WOOD], 110);
            Assert.AreEqual(cost2.GetCost()[Resource.POPULATION], 100);
        }
    }
}
