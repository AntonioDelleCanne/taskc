﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PoleisKriegAmeri.race;

/**
 * Interfaccia e rispettiva classe non mia, ma implementate perchè vengono usate in Unit.
 */
namespace PoleisKriegAmeri.player
{
    internal interface IPlayer
    {
        string Name { get; }
        int Id { get; }
        IRace PlayerRace { get; }
        //Objective Objective { get; }
    }
}
