﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PoleisKriegAmeri.race;

namespace PoleisKriegAmeri.player
{
    internal class Player : IPlayer
    {
        private readonly string _name;
        private readonly int _id;
        private readonly IRace _race;

        public string Name => _name;
        public int Id => _id;
        public IRace PlayerRace => _race;

        public Player(string name, int id, IRace race)
        {
            this._name = name;
            this._id = id;
            this._race = race;
        }
    }
}
