﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PoleisKriegAmeri.resource;

namespace PoleisKriegAmeri.cost
{
    public class BasicCost : Cost
    {
        public BasicCost(int gold, int wood, int population) : base(CreateCostMap(gold, wood, population))
        { }

        public BasicCost() : base ()
        { }

        private static Dictionary<Resource, int> CreateCostMap (int gold, int wood, int population)
        {
            Dictionary<Resource, int> map = new Dictionary<Resource, int>();
            VerifyAndAdd(map, Resource.GOLD, gold);
            VerifyAndAdd(map, Resource.WOOD, wood);
            VerifyAndAdd(map, Resource.POPULATION, population);
            return map;
        }

        private static void VerifyAndAdd(Dictionary<Resource, int> map, Resource resource, int value)
        {
            if (value != 0 ) {
                if (map.ContainsKey(resource))
                {
                    map[resource] += value;
                }
                else
                {
                    map.Add(resource, value);
                }
            }
        }
                
        public BasicCost MergeBasicCost(ICost costToMerge)
        {
            Dictionary<Resource, int> cost;
            if (costToMerge.GetCost().Count != 0)
            {
                cost = GetCost();
                costToMerge.GetCost().ToList().ForEach(e => VerifyAndAdd(cost, e.Key, e.Value));
                if (cost.Count == 0)
                {
                    return new BasicCost();
                }
            }
            return this;
        }
    }
}
