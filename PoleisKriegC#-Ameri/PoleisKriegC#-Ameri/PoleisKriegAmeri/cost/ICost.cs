﻿using System;
using System.Collections.Generic;
using System.Text;
using PoleisKriegAmeri.resource;

namespace PoleisKriegAmeri.cost
{
    public interface ICost
    {
        Dictionary<Resource, int> GetCost(); //altrimenti non posso usare virtual nelle property
        string ToString();
    }
}
