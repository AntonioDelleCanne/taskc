﻿using System.Collections.Generic;
using System.Linq;
using PoleisKriegAmeri.resource;

namespace PoleisKriegAmeri.cost
{
    public class Cost : ICost
    {
        private static readonly string FREE = "Free";
        private readonly Dictionary<Resource, int> _costMap;

        public Cost() : this(new Dictionary<Resource, int>())
        { }

        public Cost(Dictionary<Resource, int> cost)
        {
            this._costMap = cost;
        }

        public virtual Dictionary<Resource, int> GetCost()
        {
            return this._costMap;
        }

        public override string ToString()
        {
            if (_costMap.Count == 0 || AllZero())
            {
                return FREE;
            }
            else
            {
                string result = "";
                bool first = true;
                foreach (Resource r in _costMap.Keys.ToList().OrderBy(r => r.ToString()))
                {
                    if(_costMap[r] != 0)
                    {
                        if (!first)
                        {
                            result += (", ");
                        }
                        else
                        {
                            first = false;
                        }
                        result += (_costMap[r] + " " + r.ToString());
                    }
                    
                }
                return result;
            }
        }

        private bool AllZero()
        {
            return _costMap.ToList().Where(e => e.Value != 0).ToList().Count == 0;
        }

    }
}
