﻿using System;

namespace Progetto.Resources
{
    public class Population : IResource
    {
        private readonly string name;
        private readonly int? modifier;

        public Population()
        {
            name = "Population";
            modifier = null;
        }

        public string GetName()
        {
            return name;
        }

        public int? GetModifier()
        {
            return modifier;
        }

        public override bool Equals(object obj)
        {
            return name.Equals(((IResource)obj).GetName());
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
