﻿namespace Progetto.Resources
{
    public interface IResource
    {
        /// <returns>the unique name of this resource</returns>
        string GetName();

        /// <returns>the modifier to be applied at this resource if present</returns>
        int? GetModifier();
    }
}
