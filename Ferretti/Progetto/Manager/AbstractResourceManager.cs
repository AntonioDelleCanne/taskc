﻿using System;
using System.Collections.Generic;
using Progetto.Players;
using Progetto.Resources;
using System.Collections;

namespace Progetto.Manager
{
    public abstract class AbstractResourceManager : IResourceManager
    {
        private readonly Dictionary<Player, Dictionary<IResource, int>> globalResources;
        private readonly Dictionary<Player, Dictionary<IResource, int>> maxResources;

        public AbstractResourceManager(List<Player> players, int initialValues)
        {
            globalResources = new Dictionary<Player, Dictionary<IResource, int>>();
            maxResources = new Dictionary<Player, Dictionary<IResource, int>>();
            players.ForEach(p => globalResources.Add(p, GetResource(initialValues)));
            players.ForEach(p => maxResources.Add(p, GetResource(initialValues)));
        }

        public abstract void DecreaseMax(Player player, int quantity);

        public void DecreaseResource(Player player, IResource resource, int quantity)
        {
            Dictionary<IResource, int> provisory = globalResources[player];
            if (provisory[resource] - quantity < 0)
            {
                throw new System.InvalidOperationException();
            }
            else
            {
                provisory[resource] -= quantity;
            }
        }

        public Dictionary<IResource, int> GetPlayerMaxResourceMap(Player player)
        {
            return maxResources[player];
        }

        public Dictionary<IResource, int> GetPlayerResourceMap(Player player)
        {
            return globalResources[player];
        }

        public string GetPlayerResourcesInfo(Player player)
        {
            return "Resources";
        }

        public void IncreaseMax(Player player, int quantity)
        {
            Dictionary<IResource, int> provisory = maxResources[player];
            foreach (IResource res in globalResources[player].Keys)
            {
                provisory[res] += (quantity * (res.GetModifier() ?? 1));
            }
        }

        public void IncreaseResource(Player player, IResource resource, int quantity)
        {
            Dictionary<IResource, int> provisory = globalResources[player];
            if (provisory[resource] + quantity > maxResources[player][resource])
            {
                provisory[resource] = maxResources[player][resource];
            } else
            {
                provisory[resource] += quantity;
            }
        }

        public void ResetMax(Player player)
        {
            Dictionary<IResource, int> prov = maxResources[player];
            prov.Clear();
            foreach (IResource r in globalResources[player].Keys)
            {
                prov[r] = 0;
            }
        }

        protected abstract Dictionary<IResource, int> GetResource(int value);

        protected Dictionary<IResource, int> GetGlobalResources(Player player)
        {
            return globalResources[player];
        }

        protected void SetGlobalResourceQuantity(Player player, IResource resource, int quantity)
        {
            globalResources[player][resource] = quantity;
        }

        protected Dictionary<IResource, int> GetMaxResources(Player player)
        {
            return maxResources[player];
        }

        protected void SetMaxResourceQuantity(Player player, IResource res, int quantity)
        {
            maxResources[player][res] = quantity;
        }
    }
}
