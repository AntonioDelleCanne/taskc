﻿using Progetto.Resources;
using Progetto.Players;
using System.Collections.Generic;

namespace Progetto.Manager
{
    public interface IResourceManager
    {
        void IncreaseResource(Player player, IResource resource, int quantity);

        void DecreaseResource(Player player, IResource resource, int quantity);

        void IncreaseMax(Player player, int quantity);

        void DecreaseMax(Player player, int quantity);

        void ResetMax(Player player);

        Dictionary<IResource, int> GetPlayerResourceMap(Player player);

        Dictionary<IResource, int> GetPlayerMaxResourceMap(Player player);

        string GetPlayerResourcesInfo(Player player);
    }
}
