﻿using Progetto.Players;

namespace Progetto.Manager
{
    interface ITurnManager
    {
        void NextTurn();

        Player GetWinner();

        Player GetTurnPlayer();

        void RemovePlayer(Player player);
    }
}
