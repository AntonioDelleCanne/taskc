﻿using System.Collections.Generic;
using Progetto.Players;

namespace Progetto.Manager
{
    public class TurnManager : ITurnManager
    {
        private List<Player> inGamePlayers = new List<Player>();
        private Player actualTurnPlayer;
        private IEnumerator<Player> playerIterator;

        public TurnManager(List<Player> players)
        {
            inGamePlayers = players;
            playerIterator = inGamePlayers.GetEnumerator();
            actualTurnPlayer = playerIterator.Current;
        }

        private bool IsWinner(Player player)
        {
            return inGamePlayers.Count == 1 && inGamePlayers.Contains(player);
        }

        private void startTurn()
        {
            if (playerIterator.MoveNext())
            {
                actualTurnPlayer = playerIterator.Current;
            }
            else
            {
                playerIterator = inGamePlayers.GetEnumerator();
                playerIterator.MoveNext();
                actualTurnPlayer = playerIterator.Current;
            }
        }

        public Player GetTurnPlayer()
        {
            return actualTurnPlayer;
        }

        public Player GetWinner()
        {
            if (IsWinner(actualTurnPlayer))
            {
                return actualTurnPlayer;
            } else
            {
                return null;
            }
        }

        public void NextTurn()
        {
            startTurn();
        }

        public void RemovePlayer(Player player)
        {
            inGamePlayers.Remove(player);
            Player current = playerIterator.Current;
            playerIterator = inGamePlayers.GetEnumerator();
            while (playerIterator.Current != current)
            {
                playerIterator.MoveNext();
            }
        }
    }
}
