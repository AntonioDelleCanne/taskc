﻿using System;
using System.Collections.Generic;
using Progetto.Players;
using Progetto.Resources;
using System.Linq;

namespace Progetto.Manager
{
    public class BasicResourceManager : AbstractResourceManager
    {
        private readonly List<IResource> basicResources = new List<IResource> { new Gold(), new Wood(), new Population() };

        public BasicResourceManager(List<Player> players, int initialValues) : base(players, initialValues)
        {
        }

        public override void DecreaseMax(Player player, int quantity)
        {
            Dictionary<IResource, int> prov = base.GetMaxResources(player);
            foreach (IResource res in basicResources)
            {
                SetMaxResourceQuantity(player, res, prov[res] - quantity * (res.GetModifier() ?? 1));
                if (!res.GetName().Equals("Population") && (GetGlobalResources(player)[res]) > GetMaxResources(player)[res])
                {
                    SetGlobalResourceQuantity(player, res, prov[res]);
                }
            }
        }

        protected override Dictionary<IResource, int> GetResource(int value)
        {
            Dictionary<IResource, int> map = new Dictionary<IResource, int>();
            foreach (IResource res in basicResources)
            {
                map.Add(res, res.GetName().Equals("Population") ? 0 : value);
            }
            return map;
        }
    }
}
