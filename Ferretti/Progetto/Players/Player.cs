﻿namespace Progetto.Players
{
    public class Player
    {
        public string Name { get; }
        public int Id { get; }

        public Player(string name, int id)
        {
            this.Name = name;
            this.Id = id;
        }
    }
}
