﻿using System.Collections.Generic;
using Progetto.Resources;

namespace Progetto.Structures
{
    public class Carpentry : AbstractResourceProducer
    {
        private static readonly string TERRAIN = "Base";
        private static readonly int INITIAL_VALUE = 5000;
        private static readonly int PRODUCED_VALUE = 200;

        private readonly int total;
        private int produced;
        private int left;

        public Carpentry() : base(new Wood())
        {
            List<string> correctTerrains = new List<string>();
            correctTerrains.Add(TERRAIN);
            base.AddBuildableTerrain(correctTerrains);
            total = INITIAL_VALUE;
            left = total;
        }

        public override string GetDescription()
        {
            return "Carpentry\n" + GetOwnerName() + "\nLeft: " + left + " / " + this.total;
        }

        public override int GetQuantity()
        {
            if (this.left - this.produced < 0)
            {
                this.produced = this.left;
                this.left = 0;
                return this.produced;
            }
            this.left -= this.produced;
            return produced;
        }

        public override bool IsOver()
        {
            return left == 0;
        }

        public override int Produce(double modifier)
        {
            CheckOwner();
            produced = (int)(PRODUCED_VALUE * modifier);
            return produced;
        }
    }
}
