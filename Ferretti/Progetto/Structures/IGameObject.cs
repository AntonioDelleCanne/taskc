﻿using Progetto.Players;

namespace Progetto.Structures
{
    public interface IGameObject
    {
        Player GetOwner();

        void SetOwner(Player player);

        void RemoveOwner();

        string GetDescription();

        string GetOwnerName();
    }
}
