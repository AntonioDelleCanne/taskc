﻿using Progetto.Resources;

namespace Progetto.Structures
{
    public interface IResourceProducer : IOwnableStructure
    {
        int GetQuantity();

        IResource GetResource();
    }
}
