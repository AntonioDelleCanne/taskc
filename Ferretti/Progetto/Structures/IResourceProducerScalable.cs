﻿namespace Progetto.Structures
{
    public interface IResourceProducerScalable : IResourceProducer
    {
        bool IsOver();

        int Produce(double modifier);
    }
}
