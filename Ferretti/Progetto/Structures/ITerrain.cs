﻿namespace Progetto.Structures
{
    public interface ITerrain : IGameObject
    {
        string GetId();
    }
}
