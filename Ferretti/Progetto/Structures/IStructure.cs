﻿namespace Progetto.Structures
{
    public interface IStructure : IGameObject
    {
        bool CanBeBuilt(ITerrain terrain);
    }
}
