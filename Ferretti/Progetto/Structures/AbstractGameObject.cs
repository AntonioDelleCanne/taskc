﻿using Progetto.Players;

namespace Progetto.Structures
{
    public abstract class AbstractGameObject : IGameObject
    {
        private const string NEUTRAL = "Neutral";
        private Player owner = null;

        public abstract string GetDescription();

        public Player GetOwner()
        {
            return owner;
        }

        public string GetOwnerName()
        {
            return owner != null ? owner.Name : NEUTRAL;
        }

        public void RemoveOwner()
        {
            owner = null;
        }

        public void SetOwner(Player player)
        {
            owner = player;
        }
    }
}
