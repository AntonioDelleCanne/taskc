﻿using Progetto.Players;
using Progetto.Resources;
using System.Collections.Generic;

namespace Progetto.Structures
{
    public abstract class AbstractResourceProducer : AbstractGameObject, IResourceProducerScalable
    {
        private readonly IResource resource;
        private readonly LinkedList<string> correctTerrains = new LinkedList<string>();
        private Player conqueror = null;

        public AbstractResourceProducer(IResource res)
        {
            resource = res;
        }

        public void AddBuildableTerrain(List<string> list)
        {
            foreach (var item in list)
            {
                correctTerrains.AddLast(item);
            }
        }

        public bool CanBeBuilt(ITerrain terrain)
        {
            return correctTerrains.Contains(terrain.GetId());
        }

        public void EndConquer()
        {
            conqueror = null;
        }

        public Player GetConqueror()
        {
            return conqueror;
        }

        public abstract int GetQuantity();

        public IResource GetResource()
        {
            return resource;
        }

        public void InitiateConquer(Player player)
        {
            conqueror = player;
        }

        public abstract bool IsOver();
        
        public abstract int Produce(double modifier);

        protected void CheckOwner()
        {
            if (this.GetOwner() == null)
            {
                throw new System.InvalidOperationException("Can't produce without an owner!");
            }
        }
    }
}
