﻿using Progetto.Players;

namespace Progetto.Structures
{
    public interface IOwnableStructure : IStructure
    {
        void InitiateConquer(Player player);

        Player GetConqueror();

        void EndConquer();
    }
}
