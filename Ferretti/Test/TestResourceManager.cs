﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Progetto.Manager;
using Progetto.Players;
using Progetto.Resources;
using System.Collections.Generic;

namespace Test
{
    [TestClass]
    public class TestResourceManager
    {
        static readonly Player P1 = new Player("Andrea", 1);
        static readonly Player P2 = new Player("Antonio", 2);
        static BasicResourceManager manager = new BasicResourceManager(new List<Player> { P1, P2 }, 200);

        [TestMethod]
        public void TestResourceManagement()
        {
            Dictionary<IResource, int> m1 = manager.GetPlayerResourceMap(P1);
            // check that the resources are correctly initialized
            foreach (KeyValuePair<IResource, int> k in m1)
            {
                Assert.IsTrue(k.Key.GetName().Equals("Population") ? k.Value == 0 : k.Value == 200);
            }
            manager.ResetMax(P2);
            manager.IncreaseMax(P2, 5);
            Dictionary<IResource, int> m2 = manager.GetPlayerMaxResourceMap(P2);
            // check that the max correctly changed
            foreach (KeyValuePair<IResource, int> k in m2)
            {
                Assert.IsTrue(k.Key.GetName().Equals("Population") ? k.Value == 5 : k.Value == 2500);
            }
            manager.ResetMax(P1);
            manager.IncreaseMax(P1, 10);
            // check that the resources are the same
            foreach (KeyValuePair<IResource, int> k in manager.GetPlayerResourceMap(P1))
            {
                Assert.IsTrue(k.Key.GetName().Equals("Population") ? k.Value == 0 : k.Value == 200);
            }
            // check that resources are modified correctly
            foreach (KeyValuePair<IResource, int> k in manager.GetPlayerMaxResourceMap(P1))
            {
                manager.IncreaseResource(P1, k.Key, 200);
                Assert.AreEqual(manager.GetPlayerResourceMap(P1)[k.Key], k.Key.GetName().Equals("Population") ? manager.GetPlayerMaxResourceMap(P1)[k.Key] : 400);
            }
            // check that the max changes correctly the resources overflowing
            manager.IncreaseMax(P1, 5);
            foreach (KeyValuePair<IResource, int> k in manager.GetPlayerMaxResourceMap(P1))
            {
                manager.IncreaseResource(P1, k.Key, 10000);
                Assert.IsFalse(manager.GetPlayerResourceMap(P1)[k.Key] > 10000);
                Assert.AreEqual(manager.GetPlayerResourceMap(P1)[k.Key], k.Key.GetName().Equals("Population") ? 15 : 7500);
            }
            manager.DecreaseMax(P1, 5);
            foreach (KeyValuePair<IResource, int> k in manager.GetPlayerMaxResourceMap(P1))
            {
                if (!k.Key.GetName().Equals("Population"))  // population can overflow
                {
                    Assert.IsTrue(manager.GetPlayerResourceMap(P1)[k.Key] == manager.GetPlayerMaxResourceMap(P1)[k.Key]);
                }
            }
            // check that resources can't underflow
            foreach (KeyValuePair<IResource, int> k in manager.GetPlayerMaxResourceMap(P1))
            {
                try
                {
                    manager.DecreaseResource(P1, k.Key, k.Value + 200);
                }
                catch (System.InvalidOperationException e)
                {
                    
                }
            }
            foreach (KeyValuePair<IResource, int> k in manager.GetPlayerMaxResourceMap(P1))
            {
                manager.DecreaseResource(P1, k.Key, k.Key.GetName().Equals("Population") ? 5 : 300);
                Assert.AreNotEqual(manager.GetPlayerMaxResourceMap(P1)[k.Key], k.Key.GetName().Equals("Population") ? 15 : manager.GetPlayerResourceMap(P1)[k.Key]);
            }
        }
    }
}
