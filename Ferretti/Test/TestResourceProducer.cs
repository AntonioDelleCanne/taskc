﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Progetto.Structures;
using Progetto.Players;

namespace Test
{
    [TestClass]
    public class TestResourceProducer
    {
        static readonly double modifier = 1.5;
        static Mine mine = new Mine();
        static Carpentry carpentry = new Carpentry();
        static Player P1 = new Player("Andrea", 1);
        static Player P2 = new Player("Antonio", 2);

        [TestMethod]
        public void TestProduce()
        {
            // checking that those are neutral structures
            Assert.IsNull(mine.GetOwner());
            Assert.IsNull(carpentry.GetConqueror());
            // conquering the structures
            mine.InitiateConquer(P1);
            Assert.AreEqual(mine.GetConqueror(), P1);
            Assert.IsNull(mine.GetOwner());
            carpentry.InitiateConquer(P2);
            // setting the owners after they initiate the conquer
            mine.SetOwner(P1);
            carpentry.SetOwner(P2);
            Assert.IsTrue(mine.GetOwner() == P1);
            Assert.IsFalse(carpentry.GetOwner() == P1);
            // producing resources
            mine.Produce(modifier);
            carpentry.Produce(modifier);
            Assert.AreEqual(mine.GetQuantity(), 300);   // 200 + 50% of 200
            Assert.AreNotEqual(carpentry.GetQuantity(), 200);
            Assert.IsFalse(mine.IsOver());
            // conquering
            carpentry.InitiateConquer(P1);
            Assert.IsTrue(carpentry.GetConqueror() == P1);
            Assert.AreEqual(carpentry.GetOwner(), P2);
            carpentry.SetOwner(P1);
            Assert.AreEqual(carpentry.GetOwner(), P1);
            // producing all the left quantity to see it ends
            mine.Produce(10000);        // to be sure it ends the quantity
            Assert.IsFalse(mine.GetQuantity() > 5000);      // max quantity to be produced
            Assert.IsTrue(mine.IsOver());
            mine.Produce(modifier);
            Assert.AreEqual(mine.GetQuantity(), 0);
        }
    }
}
