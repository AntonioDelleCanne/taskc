﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Progetto.Manager;
using Progetto.Players;
using System.Collections.Generic;

namespace Test
{
    [TestClass]
    public class TestTurnManager
    {
        static Player P1 = new Player("Andrea", 1);
        static Player P2 = new Player("Antonio", 2);

        static List<Player> players = new List<Player> { P1, P2 };
        TurnManager turnManager = new TurnManager(players);

        [TestMethod]
        public void TestTurnFlow()
        {
            Assert.AreEqual(turnManager.GetWinner(), null);
            turnManager.NextTurn();
            // first turn, player 1 playing
            Assert.AreEqual(turnManager.GetTurnPlayer(), P1);
            Assert.AreNotEqual(turnManager.GetTurnPlayer(), P2);
            // checking a change of turn
            turnManager.NextTurn();
            Assert.IsFalse(turnManager.GetTurnPlayer() == P1);
            // removing the other player
            turnManager.NextTurn();
            turnManager.RemovePlayer(P2);
            Assert.AreSame(turnManager.GetTurnPlayer(), P1);
            // checkin winner
            Assert.IsTrue(turnManager.GetWinner() == P1);
        }
    }
}
